#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from typing import Dict, List, Tuple
import traceback
import glob
import json
import numpy as np
import datetime
from boxes import BoxMode
import pycocotools.mask as mask_util
import ntpath

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def read_json_file(file_path):
    with open(file_path) as json_file:
        return json.load(json_file)

def get_gt_image_id_dict(gt_json):
    gt_dict = read_json_file(gt_json)
    images = gt_dict['images']
    gt_img_id = {}
    for image in images:
        gt_img_id[image['id']] = ntpath.basename(image['file_name'])
    return gt_img_id

def get_predict_image_id_dict(predict_json):
    predicts = read_json_file(predict_json)
    predict_img_id = {}
    for det in predicts:
        if not det['image_id'] in predict_img_id:
            predict_img_id[det['image_id']] = ntpath.basename(det['image_file'])
    return predict_img_id

def check_image_id(gt_json, predict_json):
    return get_gt_image_id_dict(gt_json) == get_predict_image_id_dict(predict_json)

def find_file_by_ext(path:str, ext:str):
    for top, dirs, nondirs in os.walk(path):
        for _file in nondirs:
            _file_ext = os.path.splitext(_file)[-1]
            if _file_ext == ext:
                yield os.path.join(top, _file)

def find_all_anno_json_files(dataset_folders: List[str]) -> List[str]:
    if isinstance(dataset_folders, str):
        dataset_folders = [dataset_folders]
    all_jsons = []
    for folder in dataset_folders:
        one_folder_jsons = []
        for json_f in find_file_by_ext(folder, '.json'):
           one_folder_jsons.append(json_f)
        all_jsons.extend(one_folder_jsons)
        print('found {} jsons in "{}"'.format(len(one_folder_jsons), folder))
    return all_jsons

def _dis(a, b):
    return (((a - b) ** 2).sum()) ** 0.5


def _angle(a, b):
    return np.arccos(((a * b).sum()) / (np.linalg.norm(a) * np.linalg.norm(b)))

def pad_rectangle(rectangle, padding):
    padding = float(padding)
    w = rectangle[2] - rectangle[0]
    h = rectangle[3] - rectangle[1]
    pad_w = w * padding
    pad_h = h * padding
    new_rectangle = [
        int(rectangle[0] - pad_w / 2.0),
        int(rectangle[1] - pad_h / 2.0),
        int(rectangle[2] + pad_w / 2.0),
        int(rectangle[3] + pad_h / 2.0),
    ]
    return new_rectangle


def get_polygon_rectangle(polygon):
    """
        get polygon bouning rectangle
    """
    pts = np.array(polygon)
    rectangle = [pts[:, 0].min(), pts[:, 1].min(), pts[:, 0].max(), pts[:, 1].max()]
    return rectangle

def get_keypoints(pts):
    visibility = 2
    pts_new = []
    for x, y in pts:
        pts_new += [x, y, visibility]
    return pts_new

class LShape:
    """
        labelme shape
    """

    def __init__(self, content: Dict):
        self._points = content['points']

    @property
    def points(self):
        return self._points


class LRectangle(LShape):
    def __init__(self, content: Dict):
        super(LRectangle, self).__init__(content)

    def to_bbox(self, pad=0) -> List[int]:
        # [[x0,y0], [x1,y0]] -> [x0,y0,x1,y0]
        rect = self.points[0] + self.points[1]
        return pad_rectangle(rect, pad)


class LPolygon(LShape):
    def __init__(self, content: Dict):
        super(LPolygon, self).__init__(content)

    def to_bbox(self, pad=0) -> List[int]:
        bbox = get_polygon_rectangle(self.points)
        bbox = pad_rectangle(bbox, pad)
        return bbox

    def to_rbox(self):
        pts = np.array(self.points, np.float64)
        cx, cy = pts.mean(axis=0)
        h = max(_dis(pts[0], pts[1]), _dis(pts[2], pts[3]))
        w = max(_dis(pts[0], pts[3]), _dis(pts[1], pts[2]))

        box_vec = pts[1] - pts[0]
        _y_axis_vec = np.array([0, -1], np.float64)
        a = _angle(box_vec, _y_axis_vec)
        a = a / np.pi * 180
        if box_vec[0] > 0:
            a = -a
        return [cx, cy, w, h, a]

    def to_seg(self):
        return [item for p in self.points for item in p]

    def get_keypoints(self, visibility):
        # visibility = 2
        pts_new = []
        for x, y in self.points:
            pts_new += [x, y, visibility]
        return pts_new


def parse_label_shape(content: Dict):
    _type = content['shape_type']
    if _type == 'polygon':
        shape = LPolygon(content)
    elif _type == 'rectangle':
        shape = LRectangle(content)
    else:
        raise NotImplementedError
    return shape

def parse_labelme_shape(shape_content, anno_map, bbox_padding=None, num_keypoints=None):
    other = '__other__'
    label_name = shape_content['label']
    anno = None
    if label_name in anno_map:
        obj_info = anno_map[label_name]
    elif other in anno_map:
        obj_info = anno_map[other]
    else:
        print('not support label: {}, will skip it.'.format(label_name))
        return None

    if obj_info['ignore']:
        return anno

    anno = {
        'category_id': obj_info['category_id'],
        'iscrowd': 0,
    }
    if obj_info['rotated_box']:
        polygon = parse_label_shape(shape_content)
        assert isinstance(polygon, LPolygon)
        anno['bbox'] = polygon.to_rbox()
        anno['bbox_mode'] = BoxMode.XYWHA_ABS
    elif obj_info['segmentation']:
        polygon = parse_label_shape(shape_content)
        assert isinstance(polygon, LPolygon)
        anno['bbox'] = polygon.to_bbox()
        anno['bbox_mode'] = BoxMode.XYXY_ABS
        anno['segmentation'] = [polygon.to_seg()]
    else:
        rectangle = parse_label_shape(shape_content)
        assert isinstance(rectangle, LRectangle)
        anno['bbox'] = rectangle.to_bbox()
        anno['bbox_mode'] = BoxMode.XYXY_ABS
    return anno

def load_labelme_annos_from_folder(anno_jsons: List[str]):
    """
        return: [ori_anno, ...]
    """
    ori_annos = []
    for json_file in anno_jsons:
        with open(json_file) as f:
            ori_anno = json.load(f)
        ori_annos.append((ori_anno, json_file))
    return ori_annos

class LabelmeToCocoConverter:
    def __init__(self, anno_jsons: List[str], anno_map: Dict, bbox_padding, num_keypoints):
        assert len(anno_jsons) != 0
        self.anno_map = anno_map
        self.anno_jsons = anno_jsons
        self.bbox_padding = bbox_padding
        self.num_keypoints = num_keypoints
        self.anno_id = 1
        self.image_id = 0
        self.categories = [{'id': anno_map[name]['category_id'] if anno_map[name]['category_id'] is not None else -1, 'name': name} for name in anno_map]

    def parse_labelme_shape(self, shape_content, ori_frame_anno, anno_map, bbox_padding=None, num_keypoints=None):
        other = '__other__'
        label_name = shape_content['label']
        anno = None
        if label_name in anno_map:
            obj_info = anno_map[label_name]
        elif other in anno_map:
            obj_info = anno_map[other]
        else:
            print('not support label: {}, will skip it.'.format(label_name))
            return None

        if obj_info['ignore']:
            return anno

        anno = {
            'id': self.anno_id,
            'image_id': self.image_id,
            'category_id': obj_info['category_id'],
            'iscrowd': 0,
        }
        if obj_info['rotated_box']:
            raise NotImplementedError
        elif obj_info['segmentation']:
            polygon = parse_label_shape(shape_content)
            a = polygon.to_seg()
            RLEs = mask_util.frPyObjects([a], ori_frame_anno['imageHeight'], ori_frame_anno['imageWidth'])
            RLE = mask_util.merge(RLEs)
            area = mask_util.area(RLE)

            assert isinstance(polygon, LPolygon)
            x1, y1, x2, y2 = [round(float(x), 3) for x in polygon.to_bbox()]
            anno['bbox'] = [x1, y1, x2-x1, y2-y1]
            anno['area'] = int(area)
            anno["segmentation"] = [polygon.to_seg()]

        else:
            label_name = shape_content['label']
            if obj_info['keypoints']:
                '''
                This part is only for Carbon Bowl Detection.
                TODO: Add a general keypoint converter.
                '''
                if label_name == "cao":
                    polygon = parse_label_shape(shape_content)
                    if len(polygon.points) != num_keypoints:
                        print('len(polygon.points) != num_keypoints, \
                            which is {} vs {}'.format(len(polygon.points), num_keypoints))
                        return None
                    assert isinstance(polygon, LPolygon)
                    a = polygon.to_seg()
                    RLEs = mask_util.frPyObjects([a], ori_frame_anno['imageHeight'], ori_frame_anno['imageWidth'])
                    RLE = mask_util.merge(RLEs)
                    anno['area'] = int(mask_util.area(RLE))
                    x1, y1, x2, y2 = polygon.to_bbox(bbox_padding[label_name])
                    anno['bbox'] = [x1, y1, x2 - x1, y2 - y1]
                    anno['keypoints'] = polygon.get_keypoints(visibility=2)
                    anno['num_keypoints'] = 4
                    return anno
                elif label_name == "bowl":
                    rect = parse_label_shape(shape_content)
                    assert isinstance(rect, LRectangle)
                    x1, y1, x2, y2 = rect.to_bbox(bbox_padding[label_name])
                    anno['bbox'] = [x1, y1, x2 - x1, y2 - y1]
                    anno['area'] = (x2 - x1)*(y2 - y1)
                    anno['keypoints'] = [0, 0, 0] * num_keypoints
                    anno['num_keypoints'] = 4
                    return anno

            else:
                rectangle = parse_label_shape(shape_content)
                assert isinstance(rectangle, LRectangle)
                x1, y1, x2, y2 = rectangle.to_bbox()
                anno['bbox'] = [x1, y1, x2 - x1, y2 - y1]
                anno['area'] = (x2 - x1) * (y2 - y1)
                anno['bbox_mode'] = BoxMode.XYXY_ABS
                return anno

    def parse_labelme_annos(self, ori_annos):
        # frames = []
        coco_images = []
        coco_annotations = []
        for ori_frame_anno, json_file in ori_annos:
            try:
                labelme_shapes = ori_frame_anno['shapes']
                for shape in labelme_shapes:
                    anno = self.parse_labelme_shape(shape, ori_frame_anno, self.anno_map, self.bbox_padding, self.num_keypoints)
                    if anno:
                        self.anno_id += 1
                        coco_annotations.append(anno)
                    else:
                        print('ignore shape.label: {}, in file: {}'.format(shape['label'], json_file))

                img_dir = os.path.dirname(json_file)
                img_path = os.path.join(img_dir, os.path.basename(ori_frame_anno['imagePath']))

                coco_image = {
                    "id": self.image_id,
                    "width": ori_frame_anno['imageWidth'],
                    "height": ori_frame_anno['imageHeight'],
                    "file_name": ntpath.basename(img_path),
                }
                coco_images.append(coco_image)
            except:
                print('WARING: got error {}, in {}'.format(traceback.format_exc(), json_file))

            self.image_id += 1
        info = {
            "date_created": str(datetime.datetime.now()),
            "description": "Automatically generated COCO json file for Detectron2.",
        }
        coco_dict = {
            "info": info,
            "images": coco_images,
            "annotations": coco_annotations,
            "categories": self.categories,
            "licenses": None,
        }

        return coco_dict

    def get_frames(self, name='undefined_name'):
        ori_annos = load_labelme_annos_from_folder(self.anno_jsons)
        assert len(ori_annos) != 0
        frames = self.parse_labelme_annos(ori_annos)
        return frames
