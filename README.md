## 模型评估
```bash
python eval.py dataset_config_file coco_instances_results -catid category_id -ann_type annotation_type
```
参数含义：
- `dataset_config_file`: 模型配置的 `metadata.yaml` 文件，该脚本会读取文件中的验证集位置，格式请见 [metadata_example.yaml](metadata_example.yaml)。
- `coco_instances_results`: COCO 格式预测结果的 json 文件，格式范例请见 [coco_det_res_example.json](coco_det_res_example.json)。
- `catid`: 指定参与评测的类别 id，默认为全部类别
- `ann_type`: 标注类型，`bbox | segm | keypoints`，默认为 `bbox`。

## 输出示例
```bash
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.371
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.783
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.294
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.080
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.256
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.602
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.173
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.460
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.461
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.129
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.365
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.662
```