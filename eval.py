#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
import argparse
import logging
import yaml
import json
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from utils import *

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('dataset_config', help='dataset config yaml file. ')
    args_parser.add_argument('detections', help='Detect result json file. [coco format]')
    args_parser.add_argument("-catid", default=None, nargs='+', type=int, help="calculate AP for certain category")
    args_parser.add_argument('-ann_type', help='Annotation type [segm, bbox, keypoints]', default="bbox", required=False)
    args = args_parser.parse_args()

    with open(args.dataset_config) as f:
        metadata = yaml.load(f)
    bbox_padding = metadata.get('bbox_padding', None)
    keypoint_names = metadata.get('keypoint_names', None)

    val_dataset_loader = LabelmeToCocoConverter(
        anno_jsons=sorted(find_all_anno_json_files(metadata['dataset_anno']['val'])),
        bbox_padding=bbox_padding,
        num_keypoints=len(keypoint_names) if keypoint_names else None,
        anno_map=metadata['anno_map']
    )

    coco_gt_file = 'coco-gt.json'
    with open(coco_gt_file, 'w') as f:
        json.dump(val_dataset_loader.get_frames(), f)
    print("save coco-gt file")

    # check image id match
    if not check_image_id('coco-gt.json', args.detections):
        print("{}Warning: the image_id do not match. Evaluation result may not be correct.{}".format(bcolors.WARNING, bcolors.ENDC))

    gt = COCO(coco_gt_file)
    dt = gt.loadRes(args.detections)

    cocoEval = COCOeval(gt, dt, iouType=args.ann_type)
    if args.catid:
        cocoEval.params.catIds = [args.catid]
    cocoEval.evaluate()
    cocoEval.accumulate()
    cocoEval.summarize()
